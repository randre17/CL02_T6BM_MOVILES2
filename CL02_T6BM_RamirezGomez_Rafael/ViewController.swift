//
//  ViewController.swift
//  CL02_T6BM_RamirezGomez_Rafael
//
//  Created by Rafael Ramirez on 11/13/20.
//  Copyright © 2020 Rafael Ramirez. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, ProductoDelegate {

    @IBOutlet var tblProducto: UITableView!
    var listaProductos = [Producto]()
    let managerDB = CoreDataManager()
    var idProductoSeleccionado: Int32 = 0
    var nombreProductoSeleccionado: String = ""
    var cantSeleccionada: Int32 = 0
    var esActualizar: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recargarTabla()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaProductos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let viewCell = tableView.dequeueReusableCell(withIdentifier: "celdaProducto", for: indexPath) as! ProductoTableViewCell
        
        let producto = listaProductos[indexPath.row]
        viewCell.lblNombreProducto.text = producto.nomProducto
        viewCell.lblCantidad.text = "Cantidad: " + String(producto.cantProducto)
        
        return viewCell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            print("Eliminar Producto")
            let producto = listaProductos[indexPath.row]
            print("id: " + String(producto.idProducto))
            print("nombre de producto: " + String(producto.nomProducto ?? ""))
            managerDB.eliminarProducto(idProducto: producto.idProducto)
            
            listaProductos.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vista = segue.destination as? DetalleViewController{
            vista.listener = self
            vista.id = idProductoSeleccionado
            vista.nom = nombreProductoSeleccionado
            vista.cant = cantSeleccionada
            vista.seActualiza = esActualizar
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let producto = listaProductos[indexPath.row]
        print("Selecciono " + String(producto.nomProducto ?? "" ))
        
        idProductoSeleccionado = producto.idProducto
        nombreProductoSeleccionado = producto.nomProducto ?? ""
        cantSeleccionada = producto.cantProducto
        esActualizar = true
        
        performSegue(withIdentifier: "goDetalle", sender: nil)
    }
    
    func recargarTabla() {
        listaProductos = managerDB.obtenerProducto()
        print("Se recarga data de la tabla")
        tblProducto.reloadData()

        print("Se limpia variables")
        idProductoSeleccionado = 0
        nombreProductoSeleccionado = ""
        cantSeleccionada = 0
    }


}


//
//  ProductoTableViewCell.swift
//  CL02_T6BM_RamirezGomez_Rafael
//
//  Created by Rafael Ramirez on 11/14/20.
//  Copyright © 2020 Rafael Ramirez. All rights reserved.
//

import UIKit

class ProductoTableViewCell: UITableViewCell {

    @IBOutlet weak var lblNombreProducto: UILabel!
    @IBOutlet weak var lblCantidad: UILabel!
    
    /*
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
     */

}

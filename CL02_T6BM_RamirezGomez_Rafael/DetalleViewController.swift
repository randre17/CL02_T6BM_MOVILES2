//
//  DetalleViewController.swift
//  CL02_T6BM_RamirezGomez_Rafael
//
//  Created by Rafael Ramirez on 11/14/20.
//  Copyright © 2020 Rafael Ramirez. All rights reserved.
//

import UIKit

class DetalleViewController: UIViewController {

    @IBOutlet weak var txtIdProducto: UITextField!
    @IBOutlet weak var txtNombreProducto: UITextField!
    @IBOutlet weak var txtCantidad: UITextField!
    
    var listener : ProductoDelegate? = nil
    var id: Int32 = 0
    var nom: String = ""
    var cant: Int32 = 0
    var seActualiza: Bool!
    
    private let managerDB = CoreDataManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("DetalleViewController - id: " + String(id))
        print("DetalleViewController - producto: " + nom)
        print("DetalleViewController - cantidad: " + String(cant))
        //print("DetalleViewController - Bool: " + String(seActualiza))

        if (id != 0){
            txtIdProducto.text = String(id)
            txtNombreProducto.text = nom
            txtCantidad.text = String(cant)
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func agregar(_ sender: Any) {
        let idProducto = txtIdProducto.text ?? "-1"
        let nombreProducto = txtNombreProducto.text ?? ""
        let cantidad = txtCantidad.text ?? "-1"
        
        let idProductoInt = Int32(idProducto) ?? 0
        let cantidadInt = Int32(cantidad) ?? 0
        
        if(id == 0){
            print("Se agrego producto :D")
            managerDB.guardarProducto(idProducto: idProductoInt, nomProducto: nombreProducto, cantProducto: cantidadInt)
            //managerDB.actualizarProducto(idProducto: idProductoInt, nuevoNomProducto: nombreProducto, nuevaCantidad: cantidadInt)

        }else {
            print("Se actualizo producto")
            //Nota: El actualizar si funciona, pero se debe reiniciar el aplicativo para ver el cambio
            managerDB.actualizarProducto(idProducto: idProductoInt, nuevoNomProducto: nombreProducto, nuevaCantidad: cantidadInt)
            //print("id: " + String(idProductoInt))
            print("actualizar-producto: " + nom)
            print("actualizar-cantidad: " + String(cant))
            //managerDB.guardarProducto(idProducto: idProductoInt, nomProducto: nombreProducto, cantProducto: cantidadInt)
        }
        
        dismiss(animated: true) {
            self.listener?.recargarTabla()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

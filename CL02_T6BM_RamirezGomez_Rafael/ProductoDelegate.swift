//
//  ProductoDelegate.swift
//  CL02_T6BM_RamirezGomez_Rafael
//
//  Created by Rafael Ramirez on 11/14/20.
//  Copyright © 2020 Rafael Ramirez. All rights reserved.
//

import Foundation

protocol ProductoDelegate {
    func recargarTabla()
}

//
//  CoreDataManager.swift
//  CL02_T6BM_RamirezGomez_Rafael
//
//  Created by Rafael Ramirez on 11/13/20.
//  Copyright © 2020 Rafael Ramirez. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager{
    private let contenier : NSPersistentContainer!
    
    init(){
        contenier = NSPersistentContainer(name: "ProductoBD")
        contenier.loadPersistentStores { (stDes, err) in
            if let err = err {
                print("Ha ocurrido un error en la BD \(stDes) - \(err)")
            } else {
                print("Se ha cargado correctamente la BD")
            }
        }
    }
    
    func guardarProducto(idProducto: Int32, nomProducto:String, cantProducto: Int32){
        let context = contenier.viewContext
        let producto = Producto(context: context)
        producto.idProducto = idProducto
        producto.nomProducto = nomProducto
        producto.cantProducto = cantProducto
        
        do{
            try context.save()
            print("Se grabo correctamente el producto")
        } catch {
            print("No se pudo grabar el producto \(error)")
        }
    }
    
    func obtenerProducto() -> [Producto] {
        let context = contenier.viewContext
        let query : NSFetchRequest<Producto> = Producto.fetchRequest()
        do {
            let listaProducto = try context.fetch(query)
            return listaProducto
            print("Cargo la lista correctamente")
        } catch {
            print("Error al listar \(error)")
        }
        return []
    }
    
    func eliminarProducto(idProducto: Int32){
        let context = contenier.viewContext
        let query : NSFetchRequest<Producto> = Producto.fetchRequest()
        query.predicate = NSPredicate(format: "idProducto = %d", idProducto)
        do {
            let listaProducto = try context.fetch(query)
            if (listaProducto.count >= 1){
                context.delete(listaProducto[0])
                try context.save()
                print("Producto Eliminado")
            } else {
                print("No se encontraron registros con el idProducto")
            }
            print("Cargo la lista correctamente")
        } catch {
            print("Error al listar \(error)")
        }
    }
    
    func actualizarProducto(idProducto: Int32, nuevoNomProducto: String, nuevaCantidad: Int32){
        let context = contenier.viewContext
        let query : NSFetchRequest<Producto> = Producto.fetchRequest()
        query.predicate = NSPredicate(format: "idProducto = %d", idProducto)
        do {
            let listaProducto = try context.fetch(query)
            if (listaProducto.count == 1){
                listaProducto[0].setValue(nuevoNomProducto, forKey: "nomProducto")
                listaProducto[0].setValue(nuevaCantidad, forKey: "cantProducto")
                
                try context.save()
                print("Producto Actualizado")
            } else {
                print("No se encontraron registros con el idProducto")
            }
            print("Cargo la lista correctamente")
        } catch {
            print("Error al listar \(error)")
        }

    }
}
